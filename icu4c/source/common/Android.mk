# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


LOCAL_PATH:= $(call my-dir)

#
# Common definitions.
#

src_files := \
	appendable.cpp \
	bmpset.cpp \
	brkeng.cpp \
	brkiter.cpp \
	bytesinkutil.cpp \
	bytestream.cpp \
	bytestrie.cpp \
	bytestriebuilder.cpp \
	bytestrieiterator.cpp \
	caniter.cpp \
	characterproperties.cpp \
	chariter.cpp \
	charstr.cpp \
	cmemory.cpp \
	cstr.cpp \
	cstring.cpp \
	cwchar.cpp \
	dictbe.cpp \
	dictionarydata.cpp \
	dtintrv.cpp \
	edits.cpp \
	errorcode.cpp \
	filteredbrk.cpp \
	filterednormalizer2.cpp \
	icudataver.cpp \
	icuplug.cpp \
	loadednormalizer2impl.cpp \
	localebuilder.cpp \
	locavailable.cpp \
	locbased.cpp \
	locdispnames.cpp \
	locdspnm.cpp \
	locid.cpp \
	loclikely.cpp \
	locmap.cpp \
	locresdata.cpp \
	locutil.cpp \
	messagepattern.cpp \
	normalizer2.cpp \
	normalizer2impl.cpp \
	normlzr.cpp \
	parsepos.cpp \
	patternprops.cpp \
	pluralmap.cpp \
	propname.cpp \
	propsvec.cpp \
	punycode.cpp \
	putil.cpp \
	rbbi.cpp \
	rbbi_cache.cpp \
	rbbidata.cpp \
	rbbinode.cpp \
	rbbirb.cpp \
	rbbiscan.cpp \
	rbbisetb.cpp \
	rbbistbl.cpp \
	rbbitblb.cpp \
	resbund.cpp \
	resbund_cnv.cpp \
	resource.cpp \
	ruleiter.cpp \
	schriter.cpp \
	serv.cpp \
	servlk.cpp \
	servlkf.cpp \
	servls.cpp \
	servnotf.cpp \
	servrbf.cpp \
	servslkf.cpp \
	sharedobject.cpp \
	simpleformatter.cpp \
	static_unicode_sets.cpp \
	stringpiece.cpp \
	stringtriebuilder.cpp \
	uarrsort.cpp \
	ubidi.cpp \
	ubidi_props.cpp \
	ubidiln.cpp \
	ubiditransform.cpp \
	ubidiwrt.cpp \
	ubrk.cpp \
	ucase.cpp \
	ucasemap.cpp \
	ucasemap_titlecase_brkiter.cpp \
	ucat.cpp \
	uchar.cpp \
	ucharstrie.cpp \
	ucharstriebuilder.cpp \
	ucharstrieiterator.cpp \
	uchriter.cpp \
	ucln_cmn.cpp \
	ucmndata.cpp \
	ucnv.cpp \
	ucnv2022.cpp \
	ucnv_bld.cpp \
	ucnv_cb.cpp \
	ucnv_cnv.cpp \
	ucnv_ct.cpp \
	ucnv_err.cpp \
	ucnv_ext.cpp \
	ucnv_io.cpp \
	ucnv_lmb.cpp \
	ucnv_set.cpp \
	ucnv_u16.cpp \
	ucnv_u32.cpp \
	ucnv_u7.cpp \
	ucnv_u8.cpp \
	ucnvbocu.cpp \
	ucnvdisp.cpp \
	ucnvhz.cpp \
	ucnvisci.cpp \
	ucnvlat1.cpp \
	ucnvmbcs.cpp \
	ucnvscsu.cpp \
	ucnvsel.cpp \
	ucol_swp.cpp \
	ucptrie.cpp \
	ucurr.cpp \
	udata.cpp \
	udatamem.cpp \
	udataswp.cpp \
	uenum.cpp \
	uhash.cpp \
	uhash_us.cpp \
	uidna.cpp \
	uinit.cpp \
	uinvchar.cpp \
	uiter.cpp \
	ulist.cpp \
	uloc.cpp \
	uloc_keytype.cpp \
	uloc_tag.cpp \
	umapfile.cpp \
	umath.cpp \
	umutablecptrie.cpp \
	umutex.cpp \
	unames.cpp \
	unifiedcache.cpp \
	unifilt.cpp \
	unifunct.cpp \
	uniset.cpp \
	uniset_closure.cpp \
	uniset_props.cpp \
	unisetspan.cpp \
	unistr.cpp \
	unistr_case.cpp \
	unistr_case_locale.cpp \
	unistr_cnv.cpp \
	unistr_props.cpp \
	unistr_titlecase_brkiter.cpp \
	unorm.cpp \
	unormcmp.cpp \
	uobject.cpp \
	uprops.cpp \
	ures_cnv.cpp \
	uresbund.cpp \
	uresdata.cpp \
	usc_impl.cpp \
	uscript.cpp \
	uscript_props.cpp \
	uset.cpp \
	uset_props.cpp \
	usetiter.cpp \
	ushape.cpp \
	usprep.cpp \
	ustack.cpp \
	ustr_cnv.cpp \
	ustr_titlecase_brkiter.cpp \
	ustr_wcs.cpp \
	ustrcase.cpp \
	ustrcase_locale.cpp \
	ustrenum.cpp \
	ustrfmt.cpp \
	ustring.cpp \
	ustrtrns.cpp \
	utext.cpp \
	utf_impl.cpp \
	util.cpp \
	util_props.cpp \
	utrace.cpp \
	utrie.cpp \
	utrie2.cpp \
	utrie2_builder.cpp \
	utrie_swap.cpp \
	uts46.cpp \
	utypes.cpp \
	uvector.cpp \
	uvectr32.cpp \
	uvectr64.cpp \
	wintz.cpp


# This is the empty compiled-in icu data structure
# that we need to satisfy the linker.
src_files += ../stubdata/stubdata.cpp

c_includes := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/../i18n

# We make the ICU data directory relative to $ANDROID_ROOT on Android, so both
# device and sim builds can use the same codepath, and it's hard to break one
# without noticing because the other still works.
local_cflags := '-DICU_DATA_DIR_PREFIX_ENV_VAR="ANDROID_ROOT"'
local_cflags += '-DICU_DATA_DIR="/usr/icu"'

# bionic doesn't have <langinfo.h>.
local_cflags += -DU_HAVE_NL_LANGINFO_CODESET=0

local_cflags += -D_REENTRANT
local_cflags += -DU_COMMON_IMPLEMENTATION

local_cflags += -O3 -fvisibility=hidden

#
# Build for the target (device).
#

include $(CLEAR_VARS)
LOCAL_SRC_FILES += $(src_files)
LOCAL_C_INCLUDES += $(c_includes) $(optional_android_logging_includes)
LOCAL_CFLAGS += $(local_cflags) -DPIC -fPIC
LOCAL_SHARED_LIBRARIES += libdl $(optional_android_logging_libraries)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libicuuc
LOCAL_ADDITIONAL_DEPENDENCIES += $(LOCAL_PATH)/Android.mk
LOCAL_REQUIRED_MODULES += icu-data
# Use "-include" to not fail apps_only build.
-include abi/cpp/use_rtti.mk
-include external/stlport/libstlport.mk
include $(BUILD_SHARED_LIBRARY)

#
# Build for the host.
#

include $(CLEAR_VARS)
LOCAL_SRC_FILES += $(src_files)
LOCAL_C_INCLUDES += $(c_includes) $(optional_android_logging_includes)
LOCAL_CFLAGS += $(local_cflags)
LOCAL_SHARED_LIBRARIES += $(optional_android_logging_libraries)
LOCAL_LDLIBS += -ldl -lm -lpthread
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libicuuc-host
LOCAL_ADDITIONAL_DEPENDENCIES += $(LOCAL_PATH)/Android.mk
LOCAL_REQUIRED_MODULES += icu-data-host
LOCAL_MULTILIB := both
include $(BUILD_HOST_SHARED_LIBRARY)

#
# Build as a static library against the NDK
#

include $(CLEAR_VARS)
LOCAL_SDK_VERSION := 9
LOCAL_NDK_STL_VARIANT := stlport_static
LOCAL_C_INCLUDES += $(c_includes)
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
LOCAL_CPP_FEATURES := rtti
LOCAL_CFLAGS += $(local_cflags) -DPIC -fPIC -frtti
# Using -Os over -O3 actually cuts down the final executable size by a few dozen kilobytes
LOCAL_CFLAGS += -Os
LOCAL_EXPORT_CFLAGS += -DU_STATIC_IMPLEMENTATION=1
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libicuuc_static
LOCAL_SRC_FILES += $(src_files)
LOCAL_REQUIRED_MODULES += icu-data
include $(BUILD_STATIC_LIBRARY)
